package com.my.search.base;
import com.mongodb.MongoClient;
import com.my.service.task.utils.ReadProperties;


public class BaseMongo {
    protected static MongoClient mongoClient ;
	static {
		String[] addressList = ReadProperties.getKey("mongoaddr","mysearch.properties").split(",");
		String[] portList = ReadProperties.getKey("mongoport","mysearch.properties").split(",");
		String aimAddress = addressList[0];
		int aimPort = Integer.parseInt(portList[0]);
		mongoClient = new MongoClient(aimAddress, aimPort);
	}
}
