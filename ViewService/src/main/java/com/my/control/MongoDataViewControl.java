package com.my.control;

import com.alibaba.fastjson.JSONObject;
import com.my.service.task.entity.AnalyResult;
import com.my.service.task.entity.MultiAnalyResult;
import com.my.service.task.entity.MultiViewResultAnaly;
import com.my.service.task.entity.ViewResultAnaly;
import com.my.form.AnalyForm;
import com.my.service.MongoDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("mongoData")
@CrossOrigin
public class MongoDataViewControl {

    @Autowired
    MongoDataService mongoDataService;

    @RequestMapping(value = "resultinfoView",method = RequestMethod.POST,produces = "application/json;charset=UTF-8")
    public String resultinfoView(@RequestBody AnalyForm analyForm){
        String type = analyForm.getType();
        String result;
        List<AnalyResult> list = new ArrayList<>();
        if("yearBase".equals(type)){
            list = mongoDataService.searchYearBase();
        }else if ("useType".equals(type)){
            list = mongoDataService.searchUseType();
        }else if ("email".equals(type)){
            list = mongoDataService.searchEmail();
        }else if ("consumptionlevel".equals(type)){
            list = mongoDataService.searchConsumptionlevel();
        }else if ("carrier".equals(type)){
            list = mongoDataService.searchCarrier();
        }else if ("chaoManAndWomen".equals(type)){
            list = mongoDataService.searchChaoManAndWomen();
        }else if ("brandlike".equals(type)){
            list = mongoDataService.searchBrandlike();
        }else if("home".equals(type)){
            List<MultiAnalyResult> list2 = mongoDataService.searchHome();
            MultiViewResultAnaly viewResultAnaly = new MultiViewResultAnaly();
            List<String> infolist = new ArrayList<>();//分组list，x轴的值
            List<Long> countlist1 = new ArrayList<>();// 男性数量
            List<Long> countlist2 =new ArrayList<>();// 女性数量
            List<Long> countlist3 =new ArrayList<>();// 未知数量
            for(MultiAnalyResult analyResult:list2){
                infolist.add(analyResult.getInfo());
                countlist1.add(analyResult.getCount_1());
                countlist2.add(analyResult.getCount_2());
                countlist3.add(analyResult.getCount_3());
            }
            viewResultAnaly.setInfolist(infolist);
            viewResultAnaly.setCountlist_1(countlist1);
            viewResultAnaly.setCountlist_2(countlist2);
            viewResultAnaly.setCountlist_3(countlist3);
            result = JSONObject.toJSONString(viewResultAnaly);
            return result;
        }
        ViewResultAnaly viewResultAnaly = new ViewResultAnaly();
        List<String> infolist = new ArrayList<String>();//分组list，x轴的值
        List<Long> countlist =new ArrayList<Long>();//数量
        for(AnalyResult analyResult:list){
            infolist.add(analyResult.getInfo());
            countlist.add(analyResult.getCount());
        }
        viewResultAnaly.setInfolist(infolist);
        viewResultAnaly.setCountlist(countlist);
        result = JSONObject.toJSONString(viewResultAnaly);
        return result;
    }
}
