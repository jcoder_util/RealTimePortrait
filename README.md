# Flink实时全端用户画像系统

## 1. 项目背景
这套系统是针对电商平台用户的实时画像系统，这套系统能够承担亿级数据的分析与计算任务，实时的生产出用户的画像信息。通过画像数据的产出，
电商平台运营者可以很好的了解用户，从而进行针对性的商品推荐与广告投放，增加用户的粘度与平台的趣味性，具有非常高的商业价值。

## 2. 基础定义
全端用户的含义即为分析的对象为电商平台全渠道的用户数据，包括移动端，pc端，微信小程序，快应用等。

用户画像的数据包含两张表：

1.用户表

用户ID、用户名、密码、性别、手机号、邮箱、年龄、户籍省份、身份证编号、注册时间、收货地址、终端类型等

2.用户详情表

用户ID、学历、收入、职业、婚姻、是否有小孩、是否有车有房、使用手机品牌、消费水平等

对于单个用户来说，完整的画像数据中一部分可以基于用户自行填写的个人资料来进行获取，另一部分可以基于用户使用APP时被采集到的日志来进行
分析与判断，对于无法直接获取到的指标，可以基于机器学习的方式进行预测。


## 3. 项目技术方案
数据处理引擎：Flink

后端管理：SpringCloud + SpringBoot

前端展示：Vue.js

数据存储：HBase + HDFS，MongoDB

机器学习算法：Logistic Regression，tf-idf，K-means

## 4. 源码介绍

### 4.1 Common模块: 共有模块，包含一些公用的实体类和工具类
1.log  前端采集日志格式
```
BuyCartProductLog 用户购物车行为
CollectProductLog 用户收藏夹行为
FollowProductLog  用户关注商品行为
ScanProductLog 用户浏览商品行为
```
2.utils  工具类
```
MapUtils 返回hashmap中值最大的key
ReadProperties 读取文件中的配置信息
```
3.entity 实体类
```
AnalyResult 实体类，前端显示数据格式
ViewResultAnaly 实体类，前端显示数据格式
```
### 4.2 InfoInService模块: 把前端传过来的原始格式的日志，按照日志的类型生产到kafka的对应的topic当中

1.config
```
KafkaProducerConfig kafka生产者相关配置
TomcatConfig 配置Spring的Tomcat相关信息
```
2.control
```
InfoControl 接受前台传来的日志，生产到kafka对应的topic上面
```
3.entity 
```
测试实体类 ResultMessage
```
4.resources
```
application.properties SpringBoot与Kakfa相关配置
InfoInService.properties Kafka相关配置文件
```
### 4.3 PortraitAnalysis模块: 用户画像数据计算与分析

1.entity 用于分析的实体类
```
BaijiaInfo 败家指数相关
BrandLike 品牌信息：品牌， 数量，分组字段
CarrierInfo 运营商信息：运营商，数量，分组字段
ChaomanAndWomenInfo 潮男潮女信息：用户id，数量，潮男潮女标签，分组字段
ConsumptionLevel 消费水平信息：用户id，数量，消费水平标签，分组字段
EmailInfo 邮箱类型信息：邮箱类型，数量，分组字段
SexPredictionInfo 性别预测相关实体类
UserGroupInfo 用户分群相关信息实体类
YearBase 用户年代相关信息实体类：用户年代，分组字段，数量
```
2.kakfa 
```
KafkaEventSchema Kafka序列化器，用来将消费的Kakfa对应topic的数据，并将String 转为 KafkaEvent类型
KafkaEvent KafkaEvent实体类：具体的日志，数量（1），时间戳
```
3.kmeans 算法实现包，只是demo，相当于工具包

4.logistic 算法实现包，只是demo，相当于工具包

5.map 各个Flink任务的map逻辑

6.reduce 各个Flink任务的reduce逻辑

7.task

这里把各个task的数据输入源和输出目的地列出：

| 任务名 | 任务描述 | 输入源 | 输出目的地 |
| --- | --- |  --- | --- |
| BaijiaTask | 败家指数 | 文本文件 | HBase | 
| CarrierTask | 手机运营商 | 文本文件 | HBase | 
| SexPredictTask | 性别预测 | 文本文件 | HBase | 
| UserGroupTask | 用户分群分析 | 文本文件 | HBase | 
| BrandLikeTask | 品牌偏好 | Kafka | HBase | 
| ChaomanandwomenTask | 潮男潮女分析 | Kafka | HBase | 
| ConsumptionLevelTask | 消费水平分析 | 文本文件 | MongoDB | 
| EmailTask | 邮件运营商分析 | 文本文件 | MongoDB | 
| UserTypeTask | 终端分析 | Kafka | HBase | 
| YearBaseTask | 年代分析 | 文本文件 | MongoDB | 

其中涉及到算法预测参与的Task都是基于批处理的方式进行，其余的Task都是基于流处理的方式进行处理。


### 4.3 RegisterCenter: EurekaServer注册中心管理微服务

### 4.4 SearchInfo: 数据库CRUD逻辑，便于生成各种数据接口

### 4.5 ViewService: 基于SpringCloud之Feign服务生成RPC接口供前端进行调用
