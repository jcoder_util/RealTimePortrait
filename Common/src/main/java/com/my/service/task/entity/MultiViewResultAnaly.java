package com.my.service.task.entity;
import java.util.List;

public class MultiViewResultAnaly {
    private List<String> infolist;//分组list，x轴的值
    private List<Long> countlist_1;//数量list
    private List<Long> countlist_2;//数量list
    private List<Long> countlist_3;//数量list

    public List<Long> getCountlist_1() {
        return countlist_1;
    }

    public void setCountlist_1(List<Long> countlist_1) {
        this.countlist_1 = countlist_1;
    }

    public List<Long> getCountlist_2() {
        return countlist_2;
    }

    public void setCountlist_2(List<Long> countlist_2) {
        this.countlist_2 = countlist_2;
    }

    public List<Long> getCountlist_3() {
        return countlist_3;
    }

    public void setCountlist_3(List<Long> countlist_3) {
        this.countlist_3 = countlist_3;
    }

    private String result;
    private String typename;//标签类型名称
    private String lablevalue;//标签类型对应的值
    private  List<MultiViewResultAnaly> list;//所有标签信息

    public List<MultiViewResultAnaly> getList() {
        return list;
    }

    public void setList(List<MultiViewResultAnaly> list) {
        this.list = list;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public String getLablevalue() {
        return lablevalue;
    }

    public void setLablevalue(String lablevalue) {
        this.lablevalue = lablevalue;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<String> getInfolist() {
        return infolist;
    }

    public void setInfolist(List<String> infolist) {
        this.infolist = infolist;
    }

}
