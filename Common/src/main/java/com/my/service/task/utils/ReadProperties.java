package com.my.service.task.utils;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class ReadProperties {
    public static String getKey(String key, String propertyName){
        Config newConfig = ConfigFactory.load(propertyName);
        return newConfig.getString(key).trim();
    }
}
