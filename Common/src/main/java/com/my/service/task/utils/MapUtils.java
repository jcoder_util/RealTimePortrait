package com.my.service.task.utils;

import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class MapUtils {

    // 返回map中值最大的key
    public static String getKeyOfMaxValue(Map<String,Long> dataMap){
        if(dataMap.isEmpty()) return null;
        // 先新建一个TreeMap用于排序
        TreeMap<Long, String> map = new TreeMap<Long, String>(new Comparator<Long>() {
            public int compare(Long o1, Long o2) {
                return o1.compareTo(o2);
            }
        });
        Set<Map.Entry<String, Long>> entries = dataMap.entrySet();
        for(Map.Entry<String, Long> entry : entries){
            map.put(entry.getValue(),entry.getKey());
        }
        return map.get(map.firstKey());
    }
}
