package com.my.service.task.log;

import java.io.Serializable;

public class ScanProductLog implements Serializable {
    private int productId; // 商品id
    private int productTypeId; // 商品类别id
    private String scanTime; // 浏览时间
    private String stayTime; // 停留时间
    private int userId; // 用户id
    private int userType; // 终端类型
    private String brand; // 品牌
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
    public void setIp(String ip) {
        this.ip = ip;
    }

    private String ip; // 用户ip

    public String getIp() {
        return ip;
    }

    public int getProductId() {
        return productId;
    }

    public int getProductTypeId() {
        return productTypeId;
    }

    public String getScanTime() {
        return scanTime;
    }

    public String getStayTime() {
        return stayTime;
    }

    public int getUserId() {
        return userId;
    }

    public int getUserType() {
        return userType;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public void setProductTypeId(int productTypeId) {
        this.productTypeId = productTypeId;
    }

    public void setScanTime(String scanTime) {
        this.scanTime = scanTime;
    }

    public void setStayTime(String stayTime) {
        this.stayTime = stayTime;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setUserType(int useType) {
        this.userType = useType;
    }
}
