package com.my.service.task.entity;

public class MultiAnalyResult {
    private String info;//分组条件
    private Long count_1;//总数1
    private Long count_2;//总数2
    private Long count_3;//总数3

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Long getCount_1() {
        return count_1;
    }

    public void setCount_1(Long count_1) {
        this.count_1 = count_1;
    }

    public Long getCount_2() {
        return count_2;
    }

    public void setCount_2(Long count_2) {
        this.count_2 = count_2;
    }

    public Long getCount_3() {
        return count_3;
    }

    public void setCount_3(Long count_3) {
        this.count_3 = count_3;
    }
}
