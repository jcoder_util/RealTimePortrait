package com.my.service.task.reduce;

import com.my.service.task.entity.UserTypeInfo;
import org.apache.flink.api.common.functions.ReduceFunction;

public class UserTypeReducer implements ReduceFunction<UserTypeInfo> {

    public UserTypeInfo reduce(UserTypeInfo t1, UserTypeInfo t2) throws Exception {
        Long cnt1 = t1.getCount();
        Long cnt2 = t2.getCount();
        UserTypeInfo res = new UserTypeInfo();
        res.setCount(cnt1 + cnt2);
        res.setUserType(t1.getUserType());
        res.setGroupbyfield(t1.getGroupbyfield());
        return res;
    }
}
