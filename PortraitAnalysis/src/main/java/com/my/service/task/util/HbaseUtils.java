package com.my.service.task.util;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.Map;
import java.util.Set;


public class HbaseUtils {
    private static HBaseAdmin admin = null;
    private static Configuration conn = null;

    static {
        // 创建hbase配置对象
        Configuration conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", "master");
//        try {
//            conn = ConnectionFactory.createConnection(conf);
//            // 得到管理程序
//            admin = conn.getAdmin();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }

    /**
     * 向 tablename 所代表的表的 rowkey 所在行, familyname 所在列族, 按照列名插入多条数据，数据使用datamap存储
     */

    public static void put(String tablename, String rowkey, String familyname, Map<String, String> datamap) throws Exception {
//        Table table = conn.getTable(TableName.valueOf(tablename));
//        // 将字符串转换成byte[]
//        byte[] rowkeybyte = Bytes.toBytes(rowkey);
//        Put put = new Put(rowkeybyte);
//        if (datamap != null) {
//            Set<Map.Entry<String, String>> set = datamap.entrySet();
//            for (Map.Entry<String, String> entry : set) {
//                String key = entry.getKey();
//                Object value = entry.getValue();
//                put.addColumn(Bytes.toBytes(familyname), Bytes.toBytes(key), Bytes.toBytes(value + ""));
//            }
//        }
//        table.put(put);
//        table.close();
//        System.out.println("ok");
    }

    /**
     * 向 tablename 所代表的表的 rowkey 所在行, familyname 所在列族, column 所在列 插入一条数据
     */
    public static void putdata(String tablename, String rowkey, String familyname, String column, String data) throws Exception {
//        Table table = conn.getTable(TableName.valueOf(tablename));
//        Put put = new Put(rowkey.getBytes());
//        put.addColumn(familyname.getBytes(), column.getBytes(), data.getBytes());
//        table.put(put);
    }


    /**
     * 向 tablename 所代表的表的 rowkey 所在行, familyname 所在列族, column 所在列 读出一条数据
     */
    public static String getdata(String tablename, String rowkey, String familyname, String column) throws Exception {
//        Table table = conn.getTable(TableName.valueOf(tablename));
//        // 将字符串转换成byte[]
//        byte[] rowkeybyte = Bytes.toBytes(rowkey);
//        Get get = new Get(rowkeybyte);
//        System.out.println(get.toJSON());
//        Result result = table.get(get);
//        byte[] resultbytes = result.getValue(familyname.getBytes(), column.getBytes());
//
//        if (resultbytes == null) {
//            return null;
//        }
//        return new String(resultbytes);
        return "";
    }

}