package com.my.service.task;

import com.my.service.task.entity.YearBase;
import com.my.service.task.map.YearBaseMap;
import com.my.service.task.reduce.YearBaseReducer;
import com.my.service.task.util.MongoUtils;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.utils.ParameterTool;
import org.bson.Document;

import java.util.List;

public class YearBaseTask {
    public static void main(String[] args) throws Exception {
        final ParameterTool params = ParameterTool.fromArgs(args);
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        env.getConfig().setGlobalJobParameters(params);
        DataSet<String> text = env.readTextFile(params.get("input"));
        DataSet<YearBase> mapResult = text.map(new YearBaseMap());
        DataSet<YearBase> reduceResult = mapResult.groupBy("groupfield").reduce(new YearBaseReducer());
        List<YearBase> resultList = reduceResult.collect();
        for (YearBase yb : resultList) {
            String yearType = yb.getYeartype();
            Long count = yb.getCount();
            Document res = MongoUtils.findOneBy("yearbasestatics",
                    "realtimeportrait",
                    yearType
            );
            if (res == null) {
                res = new Document();
                res.put("yearbasetype", yearType);
                res.put("count", count);
            } else {
                Long countPre = res.getLong("count");
                Long total = countPre + count;
                res.put("count", total);
            }
            MongoUtils.saveOrUpdateMongo(
                    "yearbasestatics",
                    "realtimeportrait",
                    res
            );
        }
        env.execute("year base");
    }
}
