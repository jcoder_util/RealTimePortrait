package com.my.service.task.map;
import com.my.service.task.entity.YearBase;
import com.my.service.task.util.DateUtils;
import com.my.service.task.util.HbaseUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.functions.MapFunction;

public class YearBaseMap implements MapFunction<String, YearBase> {

    public YearBase map(String s) throws Exception {
        if(StringUtils.isBlank(s)) return null;
        String[] userinfos = s.split(",");
        String userid = userinfos[0];
        String username = userinfos[1];
        String sex = userinfos[2];
        String telphone = userinfos[3];
        String email = userinfos[4];
        String age = userinfos[5];
        String registerTime = userinfos[6];
        // 0、pc端：1、移动端：2、小程序端
        String usetype = userinfos[7];
        // 获取当前用户的年代标签
        String yearbasebyAge = DateUtils.getYearbasebyAge(age);
        String tablename = "userflaginfo";
        String rowkey = userid;
        String familyname = "baseinfo";
        String column = "yearbase";
        String data = yearbasebyAge;
        HbaseUtils.putdata(tablename, rowkey, familyname, column, data);
        YearBase yb = new YearBase();
        String groupfield = "yearbase == " + yearbasebyAge;
        yb.setGroupfield(groupfield);
        yb.setYeartype(yearbasebyAge);
        yb.setCount(1L);
        return yb;

    }
}
