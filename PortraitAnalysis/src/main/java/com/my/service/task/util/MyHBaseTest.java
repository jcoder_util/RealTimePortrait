package com.my.service.task.util;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

public class MyHBaseTest {

    public static Configuration conf = null;
    public static HBaseAdmin admin = null;


    public static void insertData(String tablename, String rowkey, String familyname, String column, String data) throws IOException {
        HTable table = new HTable(conf,tablename);
        Put put = new Put(rowkey.getBytes());
        put.add(familyname.getBytes(),column.getBytes(),data.getBytes());
        table.put(put);
    }


    public static String getData(String tablename, String rowkey, String familyname, String column) throws IOException {
        HTable table = new HTable(conf,tablename);
        Get get = new Get(rowkey.getBytes());
        Result res = table.get(get);
        Cell cell = res.getColumnLatestCell(familyname.getBytes(), column.getBytes());
        return Bytes.toString(CellUtil.cloneValue(cell));

    }

    public static void main(String[] args) throws IOException {
        // 1.连接 Hbase
        conf = new Configuration();
        conf.set("hbase.zookeeper.quorum","master");
        // 2.表的管理类
        admin = new HBaseAdmin(conf);
        String tablename = "userflaginfo";
        String rowkey = "123";
        String famliyname = "total";
        String colum = "ss";
        String res = getData(tablename, rowkey,famliyname,colum);
        System.out.println(res);
    }
}
