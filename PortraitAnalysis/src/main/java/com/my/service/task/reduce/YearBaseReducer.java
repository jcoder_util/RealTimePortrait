package com.my.service.task.reduce;

import com.my.service.task.entity.YearBase;
import org.apache.flink.api.common.functions.ReduceFunction;

public class YearBaseReducer implements ReduceFunction<YearBase> {

    public YearBase reduce(YearBase yearBase, YearBase t1) throws Exception {
        Long cnt1 = yearBase.getCount();
        Long cnt2 = t1.getCount();
        YearBase res = new YearBase();
        res.setCount(cnt1 + cnt2);
        res.setYeartype(yearBase.getYeartype());
        res.setGroupfield(yearBase.getGroupfield());
        return res;
    }
}
