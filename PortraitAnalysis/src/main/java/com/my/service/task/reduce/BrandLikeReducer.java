package com.my.service.task.reduce;

import com.my.service.task.entity.BrandLike;
import com.my.service.task.entity.CarrierInfo;
import org.apache.flink.api.common.functions.ReduceFunction;

public class BrandLikeReducer implements ReduceFunction<BrandLike> {

    public BrandLike reduce(BrandLike t1, BrandLike t2) throws Exception {
        Long cnt1 = t1.getCount();
        Long cnt2 = t2.getCount();
        BrandLike res = new BrandLike();
        res.setCount(cnt1 + cnt2);
        res.setBrand(t1.getBrand());
        res.setGroupbyfield(t1.getGroupbyfield());
        return res;
    }
}
