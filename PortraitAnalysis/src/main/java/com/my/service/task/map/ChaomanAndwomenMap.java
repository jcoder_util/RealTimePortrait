package com.my.service.task.map;

import com.alibaba.fastjson.JSONObject;
import com.my.service.task.entity.ChaomanAndWomenInfo;
import com.my.service.task.kafka.KafkaEvent;
import com.my.service.task.log.ScanProductLog;
import com.my.service.task.utils.ReadProperties;
import org.apache.commons.lang.StringUtils;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.util.Collector;



public class ChaomanAndwomenMap implements FlatMapFunction<KafkaEvent, ChaomanAndWomenInfo>  {

    @Override
    public void flatMap(KafkaEvent kafkaEvent, Collector<ChaomanAndWomenInfo> collector) throws Exception {
            String data = kafkaEvent.getWord();
            ScanProductLog scanProductLog = JSONObject.parseObject(data,ScanProductLog.class);
            int userid = scanProductLog.getUserId();
            int productid = scanProductLog.getProductId();
            ChaomanAndWomenInfo chaomanAndWomenInfo = new ChaomanAndWomenInfo();
            chaomanAndWomenInfo.setUserid(userid+"");
            String chaotype = ReadProperties.getKey(productid+"","productChaoLiudic.properties");
            if(StringUtils.isNotBlank(chaotype)){
                chaomanAndWomenInfo.setChaotype(chaotype);
                chaomanAndWomenInfo.setCount(1l);
                chaomanAndWomenInfo.setGroupbyfield("chaomanAndWomen=="+userid);
                collector.collect(chaomanAndWomenInfo);
            }

    }

}
