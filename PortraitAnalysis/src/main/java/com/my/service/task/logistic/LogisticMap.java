package com.my.service.task.logistic;
import com.my.service.task.entity.CarrierInfo;
import com.my.service.task.util.CarrierUtils;
import com.my.service.task.util.HbaseUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.functions.MapFunction;

import java.util.Random;

public class LogisticMap implements MapFunction<String, LogisticInfo> {

    public LogisticInfo map(String s) throws Exception {
        if(StringUtils.isBlank(s)) return null;
        String[] temps = s.split(",");
        String var1 = temps[0];
        String var2 = temps[1];
        String var3 = temps[2];
        String label = temps[3];
        LogisticInfo lInfo = new LogisticInfo();
        lInfo.setVar1(var1);
        lInfo.setVar2(var2);
        lInfo.setVar3(var3);
        lInfo.setLabel(label);
        // 将用户的数据集拆分成10组
        // 先新建一个随机数生成器
        Random random = new Random();
        lInfo.setGroupbyfield("logistic==" + random.nextInt(10));
        return lInfo;
    }
}
