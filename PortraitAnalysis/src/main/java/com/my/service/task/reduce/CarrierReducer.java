package com.my.service.task.reduce;

import com.my.service.task.entity.CarrierInfo;
import org.apache.flink.api.common.functions.ReduceFunction;

public class CarrierReducer implements ReduceFunction<CarrierInfo> {

    public CarrierInfo reduce(CarrierInfo t1, CarrierInfo t2) throws Exception {
        Long cnt1 = t1.getCount();
        Long cnt2 = t2.getCount();
        CarrierInfo res = new CarrierInfo();
        res.setCount(cnt1 + cnt2);
        res.setCarrier(t1.getCarrier());
        res.setGroupfield(t1.getGroupfield());
        return res;
    }
}
