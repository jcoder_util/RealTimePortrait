package com.my.service.task.map;
import com.my.service.task.entity.EmailInfo;
import com.my.service.task.util.EmailUtils;
import com.my.service.task.util.HbaseUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.functions.MapFunction;

public class EmailMap implements MapFunction<String, EmailInfo> {

    public EmailInfo map(String s) throws Exception {
        if(StringUtils.isBlank(s)) return null;
        String[] userinfos = s.split(",");
        String userid = userinfos[0];
        String username = userinfos[1];
        String sex = userinfos[2];
        String telphone = userinfos[3];
        String email = userinfos[4];
        String age = userinfos[5];
        String registerTime = userinfos[6];
        // 0、pc端：1、移动端：2、小程序端
        String usetype = userinfos[7];
        // 获取当前用户的年代标签
        String emailType = EmailUtils.getEmailtypeBy(email);
        String tablename = "userflaginfo";
        String rowkey = userid;
        String familyname = "baseinfo";
        String column = "emailinfo";
        HbaseUtils.putdata(tablename, rowkey, familyname, column, emailType);
        EmailInfo eifo = new EmailInfo();
        String groupfield = "carrierflag == " + emailType;
        eifo.setGroupfield(groupfield);
        eifo.setEmailtype(emailType);
        eifo.setCount(1L);
        return eifo;
    }
}
