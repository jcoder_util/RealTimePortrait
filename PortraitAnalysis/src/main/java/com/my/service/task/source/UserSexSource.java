package com.my.service.task.source;

import com.my.service.task.entity.UserSex;
import org.apache.flink.streaming.api.functions.source.SourceFunction;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

public class UserSexSource implements SourceFunction<UserSex> {

    private boolean running = true;

    @Override
    public void run(SourceContext<UserSex> sourceContext) throws Exception {
        Random rand = new Random();
        while(running){
            UserSex userSex = new UserSex();
            userSex.setUserSex("" + rand.nextInt(3));
            userSex.setTimeString(new SimpleDateFormat("yyyy.MM.dd HH:mm").format(Calendar.getInstance().getTime()));
            sourceContext.collect(userSex);
            Thread.sleep(100);
        }
    }

    @Override
    public void cancel() {
        running = false;
    }
}
