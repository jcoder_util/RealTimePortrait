package com.my.service.task.reduce;

import com.my.service.task.entity.EmailInfo;
import org.apache.flink.api.common.functions.ReduceFunction;

public class EmailReducer implements ReduceFunction<EmailInfo> {

    public EmailInfo reduce(EmailInfo t1, EmailInfo t2) throws Exception {
        Long cnt1 = t1.getCount();
        Long cnt2 = t2.getCount();
        EmailInfo res = new EmailInfo();
        res.setCount(cnt1 + cnt2);
        res.setEmailtype(t1.getEmailtype());
        res.setGroupfield(t1.getGroupfield());
        return res;
    }
}
