package com.my.service.task.reduce;

import com.my.service.task.entity.BaijiaInfo;
import org.apache.flink.api.common.functions.ReduceFunction;

import java.util.ArrayList;
import java.util.List;


public class BaijiaReducer implements ReduceFunction<BaijiaInfo>{


    public BaijiaInfo reduce(BaijiaInfo baiJiaInfo, BaijiaInfo t1) throws Exception {
        String userid = baiJiaInfo.getUserid();
        List<BaijiaInfo> baijialist1 = baiJiaInfo.getList();
        List<BaijiaInfo> baijialist2 = t1.getList();
        List<BaijiaInfo> finallist = new ArrayList<>();
        finallist.addAll(baijialist1);
        finallist.addAll(baijialist2);
        BaijiaInfo baiJiaInfofinal = new BaijiaInfo();
        baiJiaInfofinal.setUserid(userid);
        baiJiaInfofinal.setList(finallist);
        return baiJiaInfofinal;
    }
}

