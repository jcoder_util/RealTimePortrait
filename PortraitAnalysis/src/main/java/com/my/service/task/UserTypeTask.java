package com.my.service.task;

import com.my.service.task.entity.BrandLike;
import com.my.service.task.entity.UserTypeInfo;
import com.my.service.task.kafka.KafkaEvent;
import com.my.service.task.kafka.KafkaEventSchema;
import com.my.service.task.map.BrandLikeMap;
import com.my.service.task.map.UserTypeMap;
import com.my.service.task.reduce.BrandLikeReducer;
import com.my.service.task.reduce.UserTypeReducer;
import com.my.service.task.sink.BrandLikeSink;
import com.my.service.task.sink.UserTypeSink;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer010;

import javax.annotation.Nullable;

public class UserTypeTask {
    public static void main(String[] args) {
        // parse input arguments
        args = new String[]{"--input-topic","scanProductLog","--bootstrap.servers","PLAINTEXT://192.168.192.32:9092","--zookeeper.connect","192.168.192.32:2181","--group.id","my"};
        final ParameterTool parameterTool = ParameterTool.fromArgs(args);

		if (parameterTool.getNumberOfParameters() < 5) {
			System.out.println("Missing parameters!\n" +
					"Usage: Kafka --input-topic <topic> --output-topic <topic> " +
					"--bootstrap.servers <kafka brokers> " +
					"--zookeeper.connect <zk quorum> --group.id <some id>");
			return;
		}

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.getConfig().disableSysoutLogging();
        env.getConfig().setRestartStrategy(RestartStrategies.fixedDelayRestart(4, 10000));
        env.enableCheckpointing(5000); // create a checkpoint every 5 seconds
        env.getConfig().setGlobalJobParameters(parameterTool); // make parameters available in the web interface
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        DataStream<KafkaEvent> input = env
                .addSource(
                        new FlinkKafkaConsumer010<>(
                                parameterTool.getRequired("input-topic"),
                                new KafkaEventSchema(),
                                parameterTool.getProperties())
                                .assignTimestampsAndWatermarks(new CustomWatermarkExtractor()));


        // 将input的KakfaEvent转为一系列的由UserTypeInfo对象组成的Stream
        // UserTypeInfo对象有三个属性
        // 1. usertype
        // 2. groupbyfield
        // 3. count
        DataStream<UserTypeInfo> brandLikeMap = input.flatMap(new UserTypeMap());
        // 1.先对key(usertype)进行分组
        // 2.timeWindowAll统计每两秒钟内的结果
        // 3.reduce 对每个品牌的count进行累加
        DataStream<UserTypeInfo> userTypeReduce = brandLikeMap.keyBy("groupbyfield").timeWindowAll(Time.seconds(2)).reduce(new UserTypeReducer());
        // Sink类 写入到外部存储之中
        userTypeReduce.addSink(new UserTypeSink());

        try {
            env.execute("usertype analysis");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static class CustomWatermarkExtractor implements AssignerWithPeriodicWatermarks<KafkaEvent> {

        private static final long serialVersionUID = -742759155861320823L;

        private long currentTimestamp = Long.MIN_VALUE;

        @Override
        public long extractTimestamp(KafkaEvent event, long previousElementTimestamp) {
            // the inputs are assumed to be of format (message,timestamp)
            this.currentTimestamp = event.getTimestamp();
            return event.getTimestamp();
        }

        @Nullable
        @Override
        public Watermark getCurrentWatermark() {
            return new Watermark(currentTimestamp == Long.MIN_VALUE ? Long.MIN_VALUE : currentTimestamp - 1);
        }
    }
}
