package com.my.service.task;

import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.api.java.operators.FlatMapOperator;
import org.apache.flink.api.java.operators.ReduceOperator;
import org.apache.flink.api.java.utils.ParameterTool;

public class Test {
    public static void main(String[] args) {
        final ParameterTool params = ParameterTool.fromArgs(args);
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        env.getConfig().setGlobalJobParameters(params);
        DataSource<String> text = env.readTextFile(params.get("inpt"));
        FlatMapOperator<String, Object> map = text.flatMap(null);
        ReduceOperator<Object> reduce = map.groupBy("groupbyfield").reduce(null);


    }
}
